install:
	@echo "Install the zabbix"
	@apt-get install -y --force-yes zabbix-server-pgsql zabbix-frontend-php || echo "ERROR: zabbix-server-pgsql zabbix-frontend-php not found" exit 1;
	@apt-get install -y --force-yes postgresql-all || echo "ERROR: postgresql-all not found" exit 1;
	@echo "Create database"
	@echo "CREATE ROLE zabbix LOGIN;" | sudo su postgres -c 'psql' || :
	@echo "ALTER ROLE zabbix WITH PASSWORD 'zabbix';" | sudo su postgres -c 'psql' || :
	@echo "CREATE DATABASE zabbix OWNER zabbix;" | sudo su postgres -c 'psql' || :
	@echo "Coping database"
	zcat /usr/share/zabbix-server-pgsql/schema.sql.gz | sudo -u zabbix psql zabbix
	zcat /usr/share/zabbix-server-pgsql/images.sql.gz | sudo -u zabbix psql zabbix
	zcat /usr/share/zabbix-server-pgsql/data.sql.gz | sudo -u zabbix psql zabbix
	@echo "Coping zabbix_server.conf"
	cp zabbix_server.conf /etc/zabbix/zabbix_server.conf
	@echo "Start zabbix"
	systemctl start zabbix-server
	systemctl enable zabbix-server
	@echo "Coping zabbix.config to nginx"
	cp zabbix.conf /etc/nginx/sites-available/
	ln -s /etc/nginx/sites-available/zabbix.conf /etc/nginx/sites-enabled/zabbix.conf || :
	cp icon-sprite.svg /usr/share/zabbix/img/
	@echo "Coping php.ini"
	cp php.ini /etc/php/7.0/fpm/
	@echo "Reload php and nginx"
	systemctl reload php7.0-fpm
	systemctl reload nginx
	@echo "╔═══╗╔══╗╔══╗─╔══╗─╔══╗╔══╗╔══╗──╔══╗╔══╗ ╔═══╗╔═══╗╔══╗╔══╗─╔╗╔╗"
	@echo "╚═╗─║║╔╗║║╔╗║─║╔╗║─╚╗╔╝╚═╗║║╔═╝──╚╗╔╝║╔═╝ ║╔═╗║║╔══╝║╔╗║║╔╗╚╗║║║║"
	@echo "─╔╝╔╝║╚╝║║╚╝╚╗║╚╝╚╗─║║───║╚╝║─────║║─║╚═╗ ║╚═╝║║╚══╗║╚╝║║║╚╗║║╚╝║"
	@echo "╔╝╔╝─║╔╗║║╔═╗║║╔═╗║─║║───║╔╗║─────║║─╚═╗║ ║╔╗╔╝║╔══╝║╔╗║║║─║║╚═╗║"
	@echo "║─╚═╗║║║║║╚═╝║║╚═╝║╔╝╚╗╔═╝║║╚═╗──╔╝╚╗╔═╝║ ║║║║─║╚══╗║║║║║╚═╝║─╔╝║"
	@echo "╚═══╝╚╝╚╝╚═══╝╚═══╝╚══╝╚══╝╚══╝──╚══╝╚══╝ ╚╝╚╝─╚═══╝╚╝╚╝╚═══╝─╚═╝"
	@echo "Congratulations all turned out now you can set the face :)"