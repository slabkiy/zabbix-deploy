# Zabbix installation

```sh
$ make install
```

### 1) "Размонтированная полка"
```sh
{bankrl2:system.run[df /srv].str(bankrl:/srv)}=0
```

### 2) Проверка контрольной суммы
```sh
{training:cksum.test1.str(etalon value)}=0
```

### 3) Веб мониторниг:
	- Настройка -> Узлы сети -> {HOST.NAME} -> веб
	- Сценарий:
		- Имя: NAME
		- Агент: Lynx 2.8.8rel.2
	- Шаги:
		- Имя: NAME
		- URL: http://host.ru/page
		- Следовать перенаправлениям: true
		- Требуемые коды состояния: 200 (400, 404, 500, и т.д.)


### 4) Нарушение работы медиасервиса
```sh
{training:web.test.fail[RecServer].last()}<>0
```
### 5) Работает ли Postgresql и mongodb
```sh
{training:net.tcp.port[127.0.0.1,27017].last()}=0 	//mongodb
{training:proc.num[postgres].last(#2)}=0 			//postgres
```

### Настройка zabbix-agent на сервере
	- server = 127.0.0.1
	- serverAvtive = 127.0.0.1


### Скрипты (Андминистрирование -> Скрипты -> Создать скрипт)
	
	- Восстановление chroot
	```sh
	$ sudo rsync -avP /srv/chroot-etalon/ /srv/chroot/
	```
	- Перезапустить recserver
	```sh
	$ service recserver restart 
	$ service recserver status
	```

```sh
ip a - узнать имя
DL : {training:net.if.in[eth2].last()}
UL : {training:net.if.out[eth2].last()}
```
							